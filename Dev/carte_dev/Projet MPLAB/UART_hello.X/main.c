#include <p32xxxx.h>
#include "types.h"

inline s8	to_uppercase(s8 c)
{
    if (c >= 97 && c <= 122)
        return c - 32;
    else
        return c;
}

void    init_clock(void)
{
    /* Unlock sequence (allow modification clock) */
    SYSKEY = 0x00000000;
    SYSKEY = 0xAA996655;
    SYSKEY = 0x556699AA;

    PB2DIV = 0x00008000;		// Set PB2DIVbits.PBDIV to 0
    while (!PB2DIVbits.PBDIVRDY);	// Wait for PBCLK2 to be stable
    SYSKEY = 0x33333333;		// Lock sequence
}

void    init_uart(void)
{
    RPD2R = 0b0001;                     // Set UART3 TX to pin RPD2
    U3RXR = 0b0000;                     // Set UART3 RX to pin RPD3
	U3CTSR = 0b0011;					// Map U3CTS (I) function onto RD0
	RPD9R = 0b0001;						// Map U3RTS (O) function onto RD9

    U3BRG = 77;                         // Set baud rate modifier
    U3MODEbits.BRGH = 0;                // 0: (U3BRG+1) * 16
    U3MODEbits.STSEL = 0;               // 1 stop bit
	
	U3MODEbits.UEN = 0b01;				// 00 = UxTX and UxRX pins are enabled and used; UxCTS and UxRTS/UxBCLK pins are controlled by corresponding bits in the PORTx register
	U3MODEbits.RTSMD = 0b00;			// Flow control mode.

    U3STAbits.URXISEL = 0b00;           // Set RX buffer interrupt mode

    U3STAbits.UTXEN = 1;                // Enable TX
    U3STAbits.URXEN = 1;                // Enable RX
    U3MODEbits.ON = 1;                  // Enable UART3
}

s8      get_char(void)
{
    s8  c;

	while(IFS4bits.U3RXIF == 0);        // Wait while interrupt flag is clear
    c = U3RXREG;                        // Save byte inside a char
    IFS4bits.U3RXIF = 0;                // Clear RX interrupt flag
    return c;
}

void    send_char(s8 c)
{
	LATHbits.LATH2 = !PORTDbits.RD0;
    while (PORTDbits.RD0 && U3STAbits.UTXBF == 1);  // Wait while CTS high or buffer is full
    U3TXREG = c;				// Write a char on TX
	LATHbits.LATH2 = 0;
}

int     main(int argc, char** argv)
{
    s8  c;
	
    init_clock();
    init_uart();

	TRISHbits.TRISH2 = 0;
	LATHbits.LATH2 = 0;
	//PORTDbits.RD9 = 1;					// Set RTS high : not ready to receive !

    while (42)
	{
	        c = get_char();
		    c = to_uppercase(c);
			send_char(c);
    }

    return EXIT_SUCCESS;
}