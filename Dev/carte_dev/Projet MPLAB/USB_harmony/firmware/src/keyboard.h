#ifndef _KEYBOARD_H
#define _KEYBOARD_H

#include <stdint.h>
#include <stdbool.h>
#include "system_config.h"
#include "usb/usb_common.h"
#include "usb/usb_chapter_9.h"
#include "usb/usb_device.h"
#include "usb/usb_device_hid.h"


// This enumeration defines the possible state of the modifier keys.
typedef enum
{
    /* ModKey is not pressed */
    KEYBOARD_MODIFIER_KEY_STATE_RELEASED,

	/* Button is pressed */
    KEYBOARD_MODIFIER_KEY_STATE_PRESSED

}KEYBOARD_MODIFIER_KEY_STATE;


// This is the definition of the key code array.
typedef struct
{
    /* Key codes */
    USB_HID_KEYBOARD_KEYPAD keyCode[6];

}KEYBOARD_KEYCODE_ARRAY;


// This is the HID Keyboard Modifier keys data type.
typedef struct
{
    union
    {
        struct
        {
            unsigned int leftCtrl   : 1;
            unsigned int leftShift  : 1;
            unsigned int leftAlt    : 1;
            unsigned int leftGui    : 1;
            unsigned int rightCtrl  : 1;
            unsigned int rightShift : 1;
            unsigned int rightAlt   : 1;
            unsigned int rightGui   : 1;
        };

        int8_t modifierkeys;
    };

} KEYBOARD_MODIFIER_KEYS;


// This is the HID Keyboard Input Report.
typedef struct
{
    uint8_t data[8];

}KEYBOARD_INPUT_REPORT;


// This is the HID Keyboard Output Report. This reports is received by the keyboard from the host.
typedef union
{
    struct
    {
        unsigned int numLock      :1;
        unsigned int capsLock     :1;
        unsigned int scrollLock   :1;
        unsigned int compose      :1;
        unsigned int kana         :1;
        unsigned int constant     :3;

    }ledState;

    uint8_t data[64];

}KEYBOARD_OUTPUT_REPORT;


// This is the HID Keyboard LED state.
typedef enum
{
    /* This is the LED OFF state */
    KEYBOARD_LED_STATE_OFF = 0,

    /* This is the LED ON state */
    KEYBOARD_LED_STATE_ON = 1

}KEYBOARD_LED_STATE;


// This function can be used by the application to create a USB HID Keyboard Input Report. The application can then send the keyboard input report to the host by using the USB_DEVICE_HID_ReportSend() function.
void KEYBOARD_InputReportCreate
(
    KEYBOARD_KEYCODE_ARRAY * keyboardKeycodeArray,
    KEYBOARD_MODIFIER_KEYS * keyboardModifierKeys,
    KEYBOARD_INPUT_REPORT * keyboardInputReport
);

#endif


    // Indicate that the Left Alt Key is pressed:    modifierkeys.leftAlt = USB_HID_KEYBOARD_MODIFIER_KEY_STATE_PRESSED;

    // Indicate the 'A' key was pressed:    keyboardKeycodeArray.keyCode[0] = 4;;