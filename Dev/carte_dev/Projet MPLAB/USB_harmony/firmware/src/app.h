#ifndef _APP_H
#define _APP_H

#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdlib.h>
#include "system_config.h"
#include "system_definitions.h"
#include "keyboard.h"


// This enumeration defines the valid application states.
typedef enum
{
    /* Application's state machine's initial state. */
    APP_STATE_INIT=0,

    /* Application waits for configuration in this state */
    APP_STATE_WAIT_FOR_CONFIGURATION,

    /* Application checks if an output report is available */
    APP_STATE_CHECK_FOR_OUTPUT_REPORT,

    /* Application updates the switch states */
    APP_STATE_SWITCH_PROCESS,

    /* Application checks if it is still configured*/
    APP_STATE_CHECK_IF_CONFIGURED,

    /* Application emulates keyboard */
    APP_STATE_EMULATE_KEYBOARD,

    /* Application error state */
    APP_STATE_ERROR

} APP_STATES;


// This structure holds the application's data. Application strings and buffers are be defined outside this structure.
typedef struct
{
    /* The application's current state */
    APP_STATES state;

    /* Handle to the device layer */
    USB_DEVICE_HANDLE deviceHandle;

    /* Application HID instance */
    USB_DEVICE_HID_INDEX hidInstance;

    /* Keyboard modifier keys*/
    KEYBOARD_MODIFIER_KEYS keyboardModifierKeys;

    /* Key code array*/
    KEYBOARD_KEYCODE_ARRAY keyCodeArray;

    /* Is device configured */
    bool isConfigured;

    /* Switch state*/
    bool ignoreSwitchPress;

    /* Tracks switch press*/
    bool isSwitchPressed;

    /* Track the send report status */
    bool isReportSentComplete;

    /* Track if a report was received */
    bool isReportReceived;

    /* USB HID current Idle */
    uint8_t idleRate;

    /* Flag determines SOF event occurrence */
    bool sofEventHasOccurred;

    /* Receive transfer handle */
    USB_DEVICE_HID_TRANSFER_HANDLE receiveTransferHandle;

    /* Send transfer handle */
    USB_DEVICE_HID_TRANSFER_HANDLE sendTransferHandle;

    /* Keycode to be sent */
    USB_HID_KEYBOARD_KEYPAD key;

    /* USB HID active Protocol */
    USB_HID_PROTOCOL_CODE activeProtocol;

    /* Switch debounce timer */
    unsigned int switchDebounceTimer;

} APP_DATA;


// This function initializes the Harmony application. It places the application in its initial state and prepares it to run
void APP_Initialize ( void );

// This routine is the Harmony Demo application's tasks function. It defines the application's state machine and core logic.
void APP_Tasks ( void );


#endif /* _APP_H */