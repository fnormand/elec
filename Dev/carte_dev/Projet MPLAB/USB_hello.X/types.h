/*
 * File:   types.h
 * Author: bocal
 *
 * Created on March 24, 2015, 3:23 PM
 */

#ifndef TYPES_H
#define	TYPES_H

#ifdef	__cplusplus
extern "C" {
#endif
    typedef unsigned char   u8;
    typedef unsigned short  u16;
    typedef unsigned long   u32;
    typedef signed char     s8;
    typedef signed short    s16;
    typedef signed long     s32;
#ifdef	__cplusplus
}
#endif

#endif	/* TYPES_H */