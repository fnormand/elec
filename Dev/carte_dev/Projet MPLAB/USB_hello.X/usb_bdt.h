/* 
 * File:   bdt.h
 * Author: lab-elec
 *
 * Created on May 11, 2015, 7:05 PM
 */


/*
 * Valid for PIC32MZ 2048 EGC 144 memory maps.
 */

#ifndef USB_BDT_H
#define	USB_BDT_H

#ifdef	__cplusplus
extern "C" {
#endif

#include "types.h"

    /* Define the amount of USB endpoints */ 
#define BDT_ENDP_NBR (16)
    /* ^-------------^ Here ^-----------^ */

    /* Dummy identifier for reserved bit fields */
#define RESERVED_BIT

#define ADDR_PHY_RAM_START ((void *)0x0UL)
#define ADDR_KSEG1_RAM_START ((void *)0xA0000000UL)

#define ADDR_KSEG1_TO_PHY_RAM(addr) ((void *)   (  ( (void *)(addr) - (ADDR_KSEG1_RAM_START - ADDR_PHY_RAM_START)) )  )
#define ADDR_PHY_TO_VIRT_RAM(addr)  ((void *)   (  ( (void *)(addr) + (ADDR_KSEG1_RAM_START - ADDR_PHY_RAM_START)) )  )

    /*  Base endpoint BD index is endp_index * 2 * 2  */
    /* Avoiding multiplications as those macros may be used with runtime values. */
#define RX_EVEN_BD(endp)    ((endp) + (endp) + (endp) + (endp) + 0)
#define RX_ODD_BD(endp)     ((endp) + (endp) + (endp) + (endp) + 1)
#define TX_EVEN_BD(endp)    ((endp) + (endp) + (endp) + (endp) + 2)
#define TX_ODD_BD(endp)     ((endp) + (endp) + (endp) + (endp) + 3)

    typedef enum {
        BDT_EN0 = 0,
        BDT_EN1,
        BDT_EN2,
        BDT_EN3,
        BDT_EN4,
        BDT_EN5,
        BDT_EN6,
        BDT_EN7,
        BDT_EN8,
        BDT_EN9,
        BDT_EN10,
        BDT_EN11,
        BDT_EN12,
        BDT_EN13,
        BDT_EN14,
        BDT_EN15
    } t_usb_bdt_endpoint;

    typedef enum {
        USB_BD_UNDEFINED,
        USB_BD_CONTROL,
        USB_BD_STATUS
    } t_usb_bd_kind;

    struct s_usb_control_bd_offt0 {
        u32 RESERVED_BIT : 2;       /* bits0-1 Reserved */
        u32 BSTALL : 1;             /* bit2 */
        u32 DTS : 1;                /* bit3 */
        u32 NINC : 1;               /* bit4 */
        u32 KEEP : 1;               /* bit5 */
        u32 DATA0_1 : 1;            /* bit6 */
        u32 UOWN : 1;               /* bit7 */
        u32 RESERVED_BIT : 8;       /* bits8-15 Reserved */
        u32 BYTE_COUNT : 10;        /* bits16-25 */
        u32 RESERVED_BIT : 6;       /* bits26-31 Reserved */
    };

    union u_usb_control_bd_offt0 {
        u32 bulk;
        struct s_usb_control_bd_offt0 bits;
    };

    typedef struct {
        union u_usb_control_bd_offt0 offset0;
        void* const BUFFER_PHY_ADDR;
    } t_usb_control_bd;

    struct s_usb_status_bd_offt0 {
        u32 RESERVED_BIT : 2;       /* bits0-1 Reserved */
        u32 PID : 4;                /* bits2-5 */
        u32 DATA0_1 : 1;            /* bit6 */
        u32 UOWN : 1;               /* bit7 */
        u32 RESERVED_BIT : 8;       /* bits8-15 Reserved */
        u32 BYTE_COUNT : 10;        /* bits16-25 */
        u32 RESERVED_BIT : 6;       /* bits26-31 Reserved */
    };

    union u_usb_status_bd_offt0 {
        u32 bulk;
        struct s_usb_status_bd_offt0 bits;
    };

    typedef struct {
        union u_usb_status_bd_offt0 offset0;
        void* const BUFFER_PHY_ADDR;
    } t_usb_status_bd;

    /* Abstract type; software will cast as status or control as needed. */
    typedef struct {
        u32 offset0;
        void* const BUFFER_PHY_ADDR;
    } t_usb_bd;

    struct s_usb_endp_buf_pair {
        u32 even;
        u32 odd;
    };

    typedef struct {
        struct s_usb_endp_buf_pair rx;
        struct s_usb_endp_buf_pair tx;
    } t_usb_endp_buffers;

    extern t_usb_bd g_usb_bdt[BDT_ENDP_NBR * 2 * 2];
    extern const t_usb_bd_kind g_usb_bdt_kinds[BDT_ENDP_NBR];
    extern t_usb_endp_buffers g_usb_endp_buffers[BDT_ENDP_NBR];


#undef RESERVED_BIT

#ifdef	__cplusplus
}
#endif

#endif	/* USB_BDT_H */

