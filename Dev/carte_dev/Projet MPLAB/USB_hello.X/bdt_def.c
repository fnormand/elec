

/*
 * Defining / initializing the BDT.
 */

#include "usb_bdt.h"



/* Software might want to know about the BDs' kinds */
const t_usb_bd_kind g_usb_bdt_kinds[BDT_ENDP_NBR] = {
    USB_BD_CONTROL /* 0th must be a control BD */

#if (BDT_ENDP_NBR > 1)
    , USB_BD_UNDEFINED
#endif

#if (BDT_ENDP_NBR > 2)
    , USB_BD_UNDEFINED
#endif

#if (BDT_ENDP_NBR > 3)
    , USB_BD_UNDEFINED
#endif

#if (BDT_ENDP_NBR > 4)
    , USB_BD_UNDEFINED
#endif

#if (BDT_ENDP_NBR > 5)
    , USB_BD_UNDEFINED
#endif

#if (BDT_ENDP_NBR > 6)
    , USB_BD_UNDEFINED
#endif

#if (BDT_ENDP_NBR > 7)
    , USB_BD_UNDEFINED
#endif

#if (BDT_ENDP_NBR > 8)
    , USB_BD_UNDEFINED
#endif

#if (BDT_ENDP_NBR > 9)
    , USB_BD_UNDEFINED
#endif

#if (BDT_ENDP_NBR > 10)
    , USB_BD_UNDEFINED
#endif

#if (BDT_ENDP_NBR > 11)
    , USB_BD_UNDEFINED
#endif

#if (BDT_ENDP_NBR > 12)
    , USB_BD_UNDEFINED
#endif

#if (BDT_ENDP_NBR > 13)
    , USB_BD_UNDEFINED
#endif

#if (BDT_ENDP_NBR > 14)
    , USB_BD_UNDEFINED
#endif

#if (BDT_ENDP_NBR > 15)
    , USB_BD_UNDEFINED
#endif
};

/* Endpoint buffers. Uninitialized : init to 0 */
t_usb_endp_buffers __attribute__((coherent)) g_usb_endp_buffers[BDT_ENDP_NBR];



/* Define as uncached ("coherent") and 512-aligned. */
/* TODO: initialization */
/* Each Endpoint has 2 pairs ((RXeven/RXodd)/(TXeven/TXodd)) of descriptors */
t_usb_bd __attribute__((coherent, aligned(512))) g_usb_bdt[BDT_ENDP_NBR * 2 * 2] = {

    /* RX even Endpoint0 */
    { 0LU, ADDR_KSEG1_TO_PHY_RAM(&g_usb_endp_buffers[BDT_EN0].rx.even)}
    /* RX odd Endpoint0 */
    ,
    { 0LU, ADDR_KSEG1_TO_PHY_RAM(&g_usb_endp_buffers[BDT_EN0].rx.odd)}
    /* TX even Endpoint0 */
    ,
    { 0LU, ADDR_KSEG1_TO_PHY_RAM(&g_usb_endp_buffers[BDT_EN0].tx.even)}
    /* TX odd Endpoint0 */
    ,
    { 0LU, ADDR_KSEG1_TO_PHY_RAM(&g_usb_endp_buffers[BDT_EN0].tx.odd)}


#if (BDT_ENDP_NBR > 1)
    , /* RX even Endpoint1 */
    { 0LU, ADDR_KSEG1_TO_PHY_RAM(&g_usb_endp_buffers[BDT_EN1].rx.even)}
    ,
    { 0LU, ADDR_KSEG1_TO_PHY_RAM(&g_usb_endp_buffers[BDT_EN1].rx.odd)}
    ,
    { 0LU, ADDR_KSEG1_TO_PHY_RAM(&g_usb_endp_buffers[BDT_EN1].tx.even)}
    ,
    { 0LU, ADDR_KSEG1_TO_PHY_RAM(&g_usb_endp_buffers[BDT_EN1].tx.odd)}
#endif

#if (BDT_ENDP_NBR > 2)
    ,
    { 0LU, ADDR_KSEG1_TO_PHY_RAM(&g_usb_endp_buffers[BDT_EN2].rx.even)}
    ,
    { 0LU, ADDR_KSEG1_TO_PHY_RAM(&g_usb_endp_buffers[BDT_EN2].rx.odd)}
    ,
    { 0LU, ADDR_KSEG1_TO_PHY_RAM(&g_usb_endp_buffers[BDT_EN2].tx.even)}
    ,
    { 0LU, ADDR_KSEG1_TO_PHY_RAM(&g_usb_endp_buffers[BDT_EN2].tx.odd)}
#endif

#if (BDT_ENDP_NBR > 3)
    ,
    { 0LU, ADDR_KSEG1_TO_PHY_RAM(&g_usb_endp_buffers[BDT_EN3].rx.even)}
    ,
    { 0LU, ADDR_KSEG1_TO_PHY_RAM(&g_usb_endp_buffers[BDT_EN3].rx.odd)}
    ,
    { 0LU, ADDR_KSEG1_TO_PHY_RAM(&g_usb_endp_buffers[BDT_EN3].tx.even)}
    ,
    { 0LU, ADDR_KSEG1_TO_PHY_RAM(&g_usb_endp_buffers[BDT_EN3].tx.odd)}
#endif

#if (BDT_ENDP_NBR > 4)
    , 
    { 0LU, ADDR_KSEG1_TO_PHY_RAM(&g_usb_endp_buffers[BDT_EN4].rx.even)} ,
    { 0LU, ADDR_KSEG1_TO_PHY_RAM(&g_usb_endp_buffers[BDT_EN4].rx.odd)} ,
    { 0LU, ADDR_KSEG1_TO_PHY_RAM(&g_usb_endp_buffers[BDT_EN4].tx.even)} ,
    { 0LU, ADDR_KSEG1_TO_PHY_RAM(&g_usb_endp_buffers[BDT_EN4].tx.odd)}
#endif

#if (BDT_ENDP_NBR > 5)
    , 
    { 0LU, ADDR_KSEG1_TO_PHY_RAM(&g_usb_endp_buffers[BDT_EN5].rx.even)} ,
    { 0LU, ADDR_KSEG1_TO_PHY_RAM(&g_usb_endp_buffers[BDT_EN5].rx.odd)} ,
    { 0LU, ADDR_KSEG1_TO_PHY_RAM(&g_usb_endp_buffers[BDT_EN5].tx.even)} ,
    { 0LU, ADDR_KSEG1_TO_PHY_RAM(&g_usb_endp_buffers[BDT_EN5].tx.odd)}
#endif

#if (BDT_ENDP_NBR > 6)
    , 
    { 0LU, ADDR_KSEG1_TO_PHY_RAM(&g_usb_endp_buffers[BDT_EN6].rx.even)} ,
    { 0LU, ADDR_KSEG1_TO_PHY_RAM(&g_usb_endp_buffers[BDT_EN6].rx.odd)} ,
    { 0LU, ADDR_KSEG1_TO_PHY_RAM(&g_usb_endp_buffers[BDT_EN6].tx.even)} ,
    { 0LU, ADDR_KSEG1_TO_PHY_RAM(&g_usb_endp_buffers[BDT_EN6].tx.odd)}
#endif

#if (BDT_ENDP_NBR > 7)
    , 
    { 0LU, ADDR_KSEG1_TO_PHY_RAM(&g_usb_endp_buffers[BDT_EN7].rx.even)} ,
    { 0LU, ADDR_KSEG1_TO_PHY_RAM(&g_usb_endp_buffers[BDT_EN7].rx.odd)} ,
    { 0LU, ADDR_KSEG1_TO_PHY_RAM(&g_usb_endp_buffers[BDT_EN7].tx.even)} ,
    { 0LU, ADDR_KSEG1_TO_PHY_RAM(&g_usb_endp_buffers[BDT_EN7].tx.odd)}
#endif

#if (BDT_ENDP_NBR > 8)
    , 
    { 0LU, ADDR_KSEG1_TO_PHY_RAM(&g_usb_endp_buffers[BDT_EN8].rx.even)} ,
    { 0LU, ADDR_KSEG1_TO_PHY_RAM(&g_usb_endp_buffers[BDT_EN8].rx.odd)} ,
    { 0LU, ADDR_KSEG1_TO_PHY_RAM(&g_usb_endp_buffers[BDT_EN8].tx.even)} ,
    { 0LU, ADDR_KSEG1_TO_PHY_RAM(&g_usb_endp_buffers[BDT_EN8].tx.odd)}
#endif

#if (BDT_ENDP_NBR > 9)
    , 
    { 0LU, ADDR_KSEG1_TO_PHY_RAM(&g_usb_endp_buffers[BDT_EN9].rx.even)} ,
    { 0LU, ADDR_KSEG1_TO_PHY_RAM(&g_usb_endp_buffers[BDT_EN9].rx.odd)} ,
    { 0LU, ADDR_KSEG1_TO_PHY_RAM(&g_usb_endp_buffers[BDT_EN9].tx.even)} ,
    { 0LU, ADDR_KSEG1_TO_PHY_RAM(&g_usb_endp_buffers[BDT_EN9].tx.odd)}
#endif

#if (BDT_ENDP_NBR > 10)
    , 
    { 0LU, ADDR_KSEG1_TO_PHY_RAM(&g_usb_endp_buffers[BDT_EN10].rx.even)} ,
    { 0LU, ADDR_KSEG1_TO_PHY_RAM(&g_usb_endp_buffers[BDT_EN10].rx.odd)} ,
    { 0LU, ADDR_KSEG1_TO_PHY_RAM(&g_usb_endp_buffers[BDT_EN10].tx.even)} ,
    { 0LU, ADDR_KSEG1_TO_PHY_RAM(&g_usb_endp_buffers[BDT_EN10].tx.odd)}
#endif

#if (BDT_ENDP_NBR > 11)
    , 
    { 0LU, ADDR_KSEG1_TO_PHY_RAM(&g_usb_endp_buffers[BDT_EN11].rx.even)} ,
    { 0LU, ADDR_KSEG1_TO_PHY_RAM(&g_usb_endp_buffers[BDT_EN11].rx.odd)} ,
    { 0LU, ADDR_KSEG1_TO_PHY_RAM(&g_usb_endp_buffers[BDT_EN11].tx.even)} ,
    { 0LU, ADDR_KSEG1_TO_PHY_RAM(&g_usb_endp_buffers[BDT_EN11].tx.odd)}
#endif

#if (BDT_ENDP_NBR > 12)
    , 
    { 0LU, ADDR_KSEG1_TO_PHY_RAM(&g_usb_endp_buffers[BDT_EN12].rx.even)} ,
    { 0LU, ADDR_KSEG1_TO_PHY_RAM(&g_usb_endp_buffers[BDT_EN12].rx.odd)} ,
    { 0LU, ADDR_KSEG1_TO_PHY_RAM(&g_usb_endp_buffers[BDT_EN12].tx.even)} ,
    { 0LU, ADDR_KSEG1_TO_PHY_RAM(&g_usb_endp_buffers[BDT_EN12].tx.odd)}
#endif

#if (BDT_ENDP_NBR > 13)
    , 
    { 0LU, ADDR_KSEG1_TO_PHY_RAM(&g_usb_endp_buffers[BDT_EN13].rx.even)} ,
    { 0LU, ADDR_KSEG1_TO_PHY_RAM(&g_usb_endp_buffers[BDT_EN13].rx.odd)} ,
    { 0LU, ADDR_KSEG1_TO_PHY_RAM(&g_usb_endp_buffers[BDT_EN13].tx.even)} ,
    { 0LU, ADDR_KSEG1_TO_PHY_RAM(&g_usb_endp_buffers[BDT_EN13].tx.odd)}
#endif

#if (BDT_ENDP_NBR > 14)
    , 
    { 0LU, ADDR_KSEG1_TO_PHY_RAM(&g_usb_endp_buffers[BDT_EN14].rx.even)} ,
    { 0LU, ADDR_KSEG1_TO_PHY_RAM(&g_usb_endp_buffers[BDT_EN14].rx.odd)} ,
    { 0LU, ADDR_KSEG1_TO_PHY_RAM(&g_usb_endp_buffers[BDT_EN14].tx.even)} ,
    { 0LU, ADDR_KSEG1_TO_PHY_RAM(&g_usb_endp_buffers[BDT_EN14].tx.odd)}
#endif

#if (BDT_ENDP_NBR > 15)
    , 
    { 0LU, ADDR_KSEG1_TO_PHY_RAM(&g_usb_endp_buffers[BDT_EN15].rx.even)} ,
    { 0LU, ADDR_KSEG1_TO_PHY_RAM(&g_usb_endp_buffers[BDT_EN15].rx.odd)} ,
    { 0LU, ADDR_KSEG1_TO_PHY_RAM(&g_usb_endp_buffers[BDT_EN15].tx.even)} ,
    { 0LU, ADDR_KSEG1_TO_PHY_RAM(&g_usb_endp_buffers[BDT_EN15].tx.odd)}
#endif
};