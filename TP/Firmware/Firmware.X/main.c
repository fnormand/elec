/*
 * File:   main.c
 * Author: bocal
 *
 * Created on April 6, 2015, 9:54 PM
 */

#include <stdio.h>
#include <stdlib.h>
#include <p32xxxx.h>
#include <plib.h>
#include "types.h"

/*
 * if PBCLK:
 * 8 000 000 / DIV / TMR_PRESC <=> TPS (ticks per sec.)
 * 2kHz as ticks per sec : TPS / 2000 = TPS2k
 * with DIV == 8 && TMR_PRESC == 64 : 7.8... (imprecise but no 16bit TMR overflow)
 * with DIV == 8 && TMR_PRESC == 16 : 31.25 (better; 16 bit TMR overflow !)
 * with DIV == 8 && TMR_PRESC == 4 : 125 (purrfekt; 16 TMR overflow.)
 */

/*
 * I.A : press timing setup ( long  : >2s ) DONE
 * I.B : setup assignement 3 with a 32bit timer DONE
 * II. Setup output compare : continuous pulses.
 *
 */


static const u16 g_Tmr1BasePeriod = 31250u; // 8MHz / DIV_2 / Presc_256 * 2 (2s period)
static const u32 g_Tmr23BasePeriod = 8000000u; // 8MHz / DIV_2 / Presc_1 (2s period)
static const u32 g_Tmr23BaseDutyPeriod = 1000u; // 8MHz / DIV_2 / Presc_1 / 4000 (1/1kHz period)
static s32 g_dutyPeriod;
static s32 g_dutyCount;
static s8 g_variation;
static u8 g_mode;
static s16 g_setting;



#define MAX_DIM_SETTING (5)
#define SLOWEST_BLINK_SETTING (0)
#define FASTEST_BLINK_SETTING (4)
#define DIM_MODE (1)
#define BLINK_MODE (~DIM_MODE)

static void setDimMode(void) {

    OC1CONCLR = OC_ON;
    T2CONCLR = T2_ON;

    g_setting = MAX_DIM_SETTING;
    g_dutyPeriod = g_Tmr23BaseDutyPeriod * g_setting;
    g_dutyCount = 1;
    g_variation = 1;

    PR2 = g_dutyPeriod;
    OC1RS = g_dutyPeriod;
    TMR2 = 0;
    LATFCLR = _PORTF_RF1_MASK;

    OC1CONSET = OC_ON;
    T2CONSET = T2_ON;
}

static void setBlinkMode(void) {

    OC1CONCLR = OC_ON;
    T2CONCLR = T2_ON;

    g_setting = SLOWEST_BLINK_SETTING;
    g_dutyPeriod = g_Tmr1BasePeriod >> g_setting;

    PR2 = g_dutyPeriod;
    TMR2 = 0;
    LATFSET = _PORTF_RF1_MASK;

    T2CONSET = T2_ON;
}

static void __ISR(_EXTERNAL_1_VECTOR, ipl3) buttonHandler(void) {

    if (mINT1GetEdgeMode() == FALLING_EDGE_INT) {
        // Press starting
        // Clear and start timer
        TMR1 = 0;
        T1CONSET = T1_ON;
        // Switch Ext Int 1 to rising edge to determine short press
        mINT1SetEdgeMode(((RISING_EDGE_INT) >> 3) & 1);
    } else {
        // It's been a SHORT press !

        // Cancel timer1
        T1CONCLR = T1_ON;
        // Switch Ext Int 1 back to falling edge.
        mINT1SetEdgeMode(((FALLING_EDGE_INT) >> 3) & 1);
        // Change blink frequency

        T2CONCLR = T2_ON; // Stop 32bit timer2-3

        if (g_mode == DIM_MODE) {
            if (--g_setting < 1)
                g_setting = MAX_DIM_SETTING;
            TMR2 = 0;
            g_dutyCount = 0;
            g_dutyPeriod = g_Tmr23BaseDutyPeriod * g_setting;
            PR2 = g_dutyPeriod;
            OC1RS = g_dutyPeriod;
            g_variation = 1;

        } else {
            if (++g_setting > FASTEST_BLINK_SETTING)
                g_setting = SLOWEST_BLINK_SETTING;
            TMR2 = 0;
            g_dutyPeriod = g_Tmr23BasePeriod >> g_setting;
            PR2 = g_dutyPeriod;
        }
        T2CONSET = T2_ON; // Resume timer2-3
    }

    mINT1ClearIntFlag();

    asm ("nop"); // Suggested by text to clear pipeline
}

/*
 * Timer1 only times the press length.
 */
static void __ISR(_TIMER_1_VECTOR, ipl2) timer1Handler(void) {

    // It's been a LONG press !
    // Disable timer, Switch Ext Int 1 back to falling edge
    // Apply side-effect (mode change).
    T1CONCLR = T1_ON;
    mINT1SetEdgeMode(((FALLING_EDGE_INT) >> 3) & 1);

    // Toggle mode !!
    g_mode = ~g_mode;
    if (g_mode == DIM_MODE) {
        setDimMode();
    } else {
        setBlinkMode();
    }


    mT1ClearIntFlag();
    asm ("nop"); // Suggested by text to clear pipeline
}

static void __ISR(_TIMER_23_VECTOR, ipl1) timer23Handler(void) {

    if (g_mode == DIM_MODE) {
        // Set LATF RF1 High (resume duty cycle)
        LATFSET = _PORTF_RF1_MASK;
    } else {
        // Toggle LATF RF1
        LATFINV = _PORTF_RF1_MASK;
    }

    mT23ClearIntFlag();
    asm("nop");
}

void __ISR(_OUTPUT_COMPARE_1_VECTOR, ipl1) OC1_IntHandler(void) {

    if (g_mode == DIM_MODE) {

        // Iteratively change duty cycle

        LATFCLR = _PORTF_RF1_MASK;
        OC1CONCLR = OC_ON;
        T2CONCLR = T2_ON;

        OC1RS = g_dutyPeriod - g_dutyCount;

        g_dutyCount += g_variation;

        if (g_dutyCount >= g_dutyPeriod) {
            g_variation = -1;
        } else if (g_dutyCount <= 1) {
            g_variation = 1;
        }

        OC1CONSET = OC_ON;
        T2CONSET = T2_ON;
    }
    mOC1ClearIntFlag();
    asm("nop");
}

static void clearWdt(void) {
    WDTCONSET = 0x01;
}

int main(int argc, char** argv) {

    (void) argc;
    (void) argv;

    INTEnableSystemMultiVectoredInt();

    g_mode = DIM_MODE;
    g_setting = MAX_DIM_SETTING;
    g_dutyPeriod = g_Tmr23BaseDutyPeriod * g_setting;
    g_dutyCount = 1;
    g_variation = 1;


    TRISFCLR = _TRISF_TRISF1_MASK; // RF1 : LED net is an output
    LATFCLR = _PORTF_RF1_MASK; // Initialize RF1 latch to state 0

    // Setup External interrupt3 on button press
    ConfigINT1(EXT_INT_ENABLE | FALLING_EDGE_INT | EXT_INT_PRI_3);

    // Setup press-length-related timer1, in off state.
    OpenTimer1(T1_OFF | T1_SOURCE_INT | T1_PS_1_256, g_Tmr1BasePeriod);
    ConfigIntTimer1(T1_INT_ON | T1_INT_PRIOR_2);

    // Setup LED blink related 32bit timer
    // Stop any 16/32-bit Timer2 operation
    T2CON = 0x0;
    // Stop any 16-bit Timer5 operation
    T3CON = 0x0;
    // Enable 32-bit mode, prescaler 1:1, internal source
    T2CONSET = T2_32BIT_MODE_ON | T2_PS_1_1 | T2_SOURCE_INT;
    TMR2 = 0; // Clear contents of the TMR2 and TMR3
    PR2 = g_dutyPeriod; // Load PR2 and PR3 registers with 32-bit value

    IEC0CLR = _IEC0_T3IE_MASK; // Disable timer23 interrupt
    IFS0CLR = _IFS0_T3IF_MASK; // Clear timer23 interrupt flag
    ConfigIntTimer23(T23_INT_ON | T23_INT_PRIOR_1);

    // Configure OC1 as continuous pulse
    OC1CON = 0; // Disable OC1 module
    OC1R = 0; // Set primary compare register (duty cycle)
    OC1RS = g_dutyPeriod; // Set secondary compare register (off duty cycle)
    IFS0CLR = _IFS0_OC1IF_MASK; // Clear OC1 interrupt flag



    ConfigIntOC1(OC_INT_ON | OC_INT_PRIOR_1); // Init OC1 interrupt
    OC1CON = OC_ON | OC_IDLE_STOP | OC_CONTINUE_PULSE | OC_TIMER_MODE32; // Configure OC1

    T2CONSET = T2_ON; // Start Timer23

    for (;;) {
        clearWdt();
    } // Clear watchdog


    return (EXIT_SUCCESS);
}

